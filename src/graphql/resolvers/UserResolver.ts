import { Args, Int, Query, Resolver } from '@nestjs/graphql';
import { User } from '../models/User';
import { mockUsers } from 'src/data/mockUsers';

@Resolver()
export class UserResolver {
  @Query(() => User)
  getUser() {
    return {
      id: 1,
      username: 'user',
      email: 'user@example.com',
      lastName: 'userName',
    };
  }

  @Query(() => User, { nullable: true })
  getUserId(@Args('id', { type: () => Int }) id: number) {
    return mockUsers.find((user) => user.id === id);
  }
}
