// array for user input
export const mockUsers = [
  {
    id: 1,
    username: 'user',
    email: 'user@example.com',
    lastName: 'userName1',
  },
  {
    id: 2,
    username: 'user2',
    email: 'user2@example.com',
    lastName: 'userName2',
  },
  {
    id: 3,
    username: 'user3',
    email: 'user3@example.com',
    lastName: 'userName3',
  },
];
