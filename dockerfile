# usar una imagen como base
FROM node:21

# establecer un directorio de trabajo
WORKDIR /app

# copiar el codigo al directorio
COPY . .

# instalar dependencias
RUN npm install

# exporner puerto
EXPOSE 3000

# comandos que ejecutaran la aplicación
CMD [ "npm", "run", "start:dev" ]
